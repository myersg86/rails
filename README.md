# rails

## Resources & Roadmap for Learning & Mastering Ruby on Rails

## Roadmap

- [Ruby on Rails Tutorial Book](https://www.railstutorial.org/book)
- [Rails Official Guide](https://guides.rubyonrails.org/getting_started.html)
- [The Odin Project](https://www.theodinproject.com/courses/ruby-on-rails)
- [ThoughtBot Intermediate Rails 5](https://thoughtbot.com/upcase/intermediate-ruby-on-rails-five)

### Official Resources

- [Rails Official Website](http://rubyonrails.org)
- [Rails Official Guide](http://guides.rubyonrails.org) <small>[(Edge Guide)](http://edgeguides.rubyonrails.org)</small>
- [Rails API Documentation](http://api.rubyonrails.org)
- [Rails Source Code](https://github.com/rails/rails)
- [Rails Official Blog](http://weblog.rubyonrails.org)

### External Resources

- [TutorialsPoint RubyOnRails](https://www.tutorialspoint.com/ruby-on-rails/)
- [RailsCasts](http://railscasts.com)
- [GoRails](https://gorails.com)
- [Drifting Ruby](https://www.driftingruby.com/)

### Tutorials

- [Ruby on Rails Code Audits: 8 Steps to Review Your App](http://blog.planetargon.com/entries/ruby-on-rails-code-audits-8-steps-to-review-your-app)
- [nil?, empty?, blank? in Ruby on Rails - what's the difference actually?](http://blog.arkency.com/2017/07/nil-empty-blank-ruby-rails-difference/)
- [The 3 Tenets of Service Objects in Ruby on Rails](https://hackernoon.com/the-3-tenets-of-service-objects-c936b891b3c2)
- [Famous Web Apps Built with Ruby on Rails](https://railsware.com/blog/2017/06/23/famous-web-apps-built-with-ruby-on-rails/)
- [Connecting to Snowflake with Ruby on Rails](http://eng.localytics.com/connecting-to-snowflake-with-ruby-on-rails/)
- [Building a JSON API with Rails 5](https://blog.codeship.com/building-a-json-api-with-rails-5/)
- [Building APIs with Ruby on Rails and GraphQL](https://www.sitepoint.com/building-apis-ruby-rails-graphql/)
- [Five Practices for Robust Ruby on Rails Applications](https://blog.codeship.com/five-practices-for-robust-ruby-on-rails-applications/)
- [8 Useful Ruby on Rails Gems We Couldn't Live Without](http://blog.planetargon.com/entries/8-useful-ruby-on-rails-gems-we-couldnt-live-without)
- [File Upload in Rails with PaperClip](https://scotch.io/tutorials/file-upload-in-rails-with-paperclip)
- [Integration Testing Ruby on Rails with Minitest and Capybara](https://semaphoreci.com/community/tutorials/integration-testing-ruby-on-rails-with-minitest-and-capybara)
- [Build a RESTful JSON API With Rails 5](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)
- [Crafting APIs With Rails](https://code.tutsplus.com/articles/crafting-apis-with-rails--cms-27695)
- [10 Ruby on Rails Best Practices](https://www.sitepoint.com/10-ruby-on-rails-best-practices-3/)
- [Build a Blog with Ruby on Rails](https://scotch.io/tutorials/build-a-blog-with-ruby-on-rails-part-1)

### Installation/Config

- [18.04 - gorails](https://gorails.com/setup/ubuntu/18.04#rails)
- [16.04 - GoRails](https://gorails.com/setup/ubuntu/16.04)
- [16.04 - DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-16-04)

### Notable Awesome Stuffs
- [rails-dashboard](https://github.com/y-takey/rails-dashboard) - A dev-tool to improve your rails log.
- [awesome-ruby](https://github.com/sdogruyol/awesome-ruby)
- [awesome-rails-gem](https://github.com/hothero/awesome-rails-gem)